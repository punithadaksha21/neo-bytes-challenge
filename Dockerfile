FROM python:3.9-alpine
WORKDIR /var/pythonsample
COPY my_first_script.py /var/pythonsample
ENTRYPOINT ["python", "my_first_script.py"]
